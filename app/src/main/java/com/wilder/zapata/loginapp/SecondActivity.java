package com.wilder.zapata.loginapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    public static final String USER_NAME = "usrname";
    public static final String NAME = "res";
    public static final String NICK = "nick";
    public static final String DATE = "date";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Bundle bundle = getIntent().getExtras();

        String user = "";

        if(bundle != null) {
            user = bundle.getString(USER_NAME, "NnN");
        }

        setTitle("Welcome " + user);

        FloatingActionButton fab = findViewById(R.id.fltBtnAdd);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View view) {
                Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                // startActivity(intent);
                startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (data != null) {
            TextView name = findViewById(R.id.txvwName);
            TextView nick = findViewById(R.id.txvwNick);
            TextView age =  findViewById(R.id.txvwAge);

            name.setText(name.getText().toString() + data.getStringExtra(NAME).toString());
            nick.setText(nick.getText().toString() + data.getStringExtra(NICK).toString());
            age.setText(age.getText().toString() + data.getStringExtra(DATE).toString());

        }
    }
}
