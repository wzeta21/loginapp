package com.wilder.zapata.loginapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {
    public final static int MY_REQUEST_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);


        Button btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });
    }
    @Override
    public void onBackPressed() {
        back();

    }
    private void back(){
        EditText name = findViewById(R.id.txtName);
        EditText nick = findViewById(R.id.txtNickName);
        EditText date = findViewById(R.id.dtpDate);

        Intent resusltData = new Intent();

        resusltData.putExtra(SecondActivity.NAME, name.getText().toString());
        resusltData.putExtra(SecondActivity.NICK, nick.getText().toString());
        resusltData.putExtra(SecondActivity.DATE, date.getText().toString());

        setResult(Activity.RESULT_OK, resusltData);
        finish();
    }
}
