package com.wilder.zapata.loginapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String message = "";
    private String usr = "wilder";
    static final String STATE_LOGIN = "loginMessages";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("states", "onCreate");
        setContentView(R.layout.activity_main);

        final TextView txtLoged = findViewById(R.id.txtLoged);

        final Button btnLogin = findViewById(R.id.btnOk);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView txtName = findViewById(R.id.txtUser);
                TextView txtPwd = findViewById(R.id.txtPwd);
                String user = txtName.getText().toString();
                String pwd = txtPwd.getText().toString();
                if(user.equals(usr) && pwd.equals("1234")){
                    alert("sucessfull");
                    txtLoged.setText("sucessfull: you are logged " + usr);
                    message = txtLoged.getText().toString();
                    loadSecondActivity();
                } else {
                    alert("Login or Password are incorrect!");
                    txtLoged.setText("Fail: Login or Password are incorrect!");
                    message = txtLoged.getText().toString();
                }
            }
        });
    }
    @Override
    protected  void onPause() {
        super.onPause();
        Log.d("states", "onPause");

    }
    @Override
    protected  void onResume() {
        super.onResume();
        Log.d("states", "onResume");

    }
    @Override
    protected  void onDestroy() {
        super.onDestroy();
        Log.d("states", "onDestroy");
        Log.d("stated", message);
    }
    @Override
    protected  void onStop() {
        super.onStop();
        Log.d("states", "onStop");
    }
    @Override
    protected  void onRestart() {
        super.onRestart();
        Log.d("states", "onRestart");
    }
    @Override
    protected void onSaveInstanceState(Bundle saveInstanceState) {
        saveInstanceState.putString(STATE_LOGIN, message);
        super.onSaveInstanceState(saveInstanceState);
        Log.d("states", "onSaveInstanceState");
    }
    @Override
    protected  void onRestoreInstanceState(Bundle saveInstanceState){
        super.onRestoreInstanceState(saveInstanceState);
        message = saveInstanceState.getString(this.STATE_LOGIN);
        final TextView txtLoged = findViewById(R.id.txtLoged);
        txtLoged.setText(message);
        Log.d("states-restare", message);
    }
    public void loadSecondActivity() {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(SecondActivity.USER_NAME, usr);
        startActivity(intent);
    }
    private void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

}
